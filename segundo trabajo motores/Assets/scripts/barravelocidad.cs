using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class barravelocidad : MonoBehaviour
{
    public Image circulito;
    public float maxvel;
    public float velact;
    public float lerpvel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lerpvel = (3f * Time.deltaTime);
        velact = controlavion.velocidad;
        llenadorcirculo();
        cambiocolor();
    }

    public void llenadorcirculo()
    {
        circulito.fillAmount = Mathf.Lerp(circulito.fillAmount , (velact / maxvel),lerpvel);
    }

    public void cambiocolor()
    {
        Color colorbarra = Color.Lerp(Color.green, Color.red, (velact / maxvel));
        circulito.color = colorbarra;
    }
}

