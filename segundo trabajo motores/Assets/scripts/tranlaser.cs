using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tranlaser : MonoBehaviour
{
    public GameObject laser;
    public GameObject yo;
    GameObject  activo;
    // Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("blasterapuntado");
        Invoke("ca�on", 1f);
        Destroy(yo, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ca�on()
    {
        GestorDeAudio.instancia.ReproducirSonido("blasterdisparado");
        activo = Instantiate(laser, yo.transform.position, yo.transform.rotation);
        Destroy(activo, 2);

    }
}
