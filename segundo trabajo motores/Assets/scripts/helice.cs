using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class helice : MonoBehaviour
{
    public GameObject codigomarcha;
    public int velocidad;
    public int x;
    public int y;
    public int z;

    void Update()
    {
        velocidad = controlavion.marcha;
        transform.Rotate(new Vector3(x, y, z)  * velocidad);
    }
}
