using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class compseñal3 : MonoBehaviour
{
    public GameObject avion;
    private Vector3 startpoint;
    private bool hecho;

    public int rapidez;

    // Start is called before the first frame update
    void Start()
    {
        startpoint = this.transform.position;
        hecho = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (controlavion.marcha == 0)
        {
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
            hecho = true;
        }
        if ((controlavion.marcha != 0) && (hecho == true))
        {
            this.transform.position = ((avion.transform.position) - (new Vector3(14,8,6)));
            hecho = false;
        }
    }
}
