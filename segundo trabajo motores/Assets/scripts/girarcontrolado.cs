using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class girarcontrolado : MonoBehaviour
{
    public float velocidad;
    public int x = 0;
    public int y = 0;
    public int z= 0;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            z-= 10;
        }
        transform.Rotate(new Vector3(x, y, z) * Time.deltaTime * velocidad);
    }
}
