using System.Collections;using System.Collections.Generic;
using UnityEngine;

public class controlavion : MonoBehaviour
{
    public  static int marcha = 0;
    public float aceleracion = 20;
    public float giroF = 120;
    private float Giro;
    Rigidbody rb;
    public GameObject pantallagameover;
    public static float velocidad;
    public TMPro.TMP_Text textovelocidad;
    public TMPro.TMP_Text textomarcha;
    public TMPro.TMP_Text textoradio;
    public TMPro.TMP_Text textocuenta;
    public TimeManager timeManager;
    public static float cuenta;
    public GameObject explosion;
    public GameObject yo;
    public GameObject yorender;
    public GameObject aguaefe;
    public GameObject bala;
    private bool sobreagua;
    GameObject gotas;
    GameObject bal;


    void Start()
    {
        cuenta = 0;
        rb = GetComponent<Rigidbody>();
        sobreagua = false;
        StartCoroutine(Comenzarcontador());

    }
    
    void Update()
    {
        setearTextos();

        velocidad = marcha * aceleracion;

        //moverse adelante
        transform.position += transform.forward * velocidad * Time.deltaTime;
        
        // flechas
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");


        // rotacion movimiento
        Giro += horizontalInput * giroF * Time.deltaTime;
        float pitch = Mathf.Lerp(0, 80,Mathf.Abs(verticalInput)) * Mathf.Sign(verticalInput);
        float roll = Mathf.Lerp(0, 90, Mathf.Abs(horizontalInput)) * -Mathf.Sign(horizontalInput);
        
        // rotacion avion
        transform.localRotation = Quaternion.Euler(Vector3.up * Giro + Vector3.right * pitch + Vector3.forward * roll);

        //vuelo o caida
        if(velocidad>0)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionY;
        }
        else
        {
            rb.constraints = RigidbodyConstraints.None;
        }

        if ((Input.GetKeyDown("e"))  && (marcha < 7))
        {
            marcha++;
        }
        if ((Input.GetKeyDown("c")) && (marcha>0) )
        {
            marcha--;
        }

        if (Input.GetKeyDown("x"))
        {
            timeManager.DoSlowmotion();
        }

        //disparo
        if (Input.GetKeyDown("i"))
        {
            bal = Instantiate(bala, (yo.transform.position + new Vector3(0, 0, 3)), yo.transform.rotation);
            Destroy(bal, 2);

        }


    }

    public IEnumerator Comenzarcontador()
    {
        while (Time.timeScale != 0)
        {
            Debug.Log("pasaron " + cuenta + " segundos.");
            yield return new WaitForSeconds(1.0f);
            cuenta++;
        }

    }

        private void setearTextos()
    {
        textovelocidad.text = "k/h:  " + velocidad.ToString();
        textomarcha.text = "Marcha:" + marcha.ToString();
        textoradio.text = "Track:" + radio.cancion;
        textocuenta.text = "duraci�n:" + cuenta.ToString();
    }

    private void perder()
    {
        GestorDeAudio.instancia.ReproducirSonido("hurt");
        pantallagameover.SetActive(true);
        Time.timeScale = 0;
    }



    //colisiones
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("piso") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            Destroy(yorender);
            Destroy(kaboom, 2f);
            Invoke("perder", 0.5f);
        }
        if (other.gameObject.CompareTag("enemigo") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            Destroy(yorender);
            Destroy(kaboom, 2f);
            Invoke("perder", 0.5f);
        }
        if (other.gameObject.CompareTag("misil") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            Destroy(yorender);
            Destroy(kaboom, 2f);
            Invoke("perder", 0.5f);
        }
        if (other.gameObject.CompareTag("laser") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            Destroy(yorender);
            Destroy(kaboom, 2f);
            Invoke("perder", 0.5f);
        }
        if ((other.gameObject.CompareTag("aguab") == true) && (sobreagua == false))
        {
            sobreagua = true;
            gotas = Instantiate(aguaefe,( yo.transform.position - new Vector3(1,1,1)), yo.transform.rotation, yo.transform);
        }
        if ((other.gameObject.CompareTag("aguac") == true) && (sobreagua == true))
        {
            Destroy(gotas);
            sobreagua = false;
        }
        if (other.gameObject.CompareTag("limite") == true)
        {
            marcha = 0;
        }
    }
}


