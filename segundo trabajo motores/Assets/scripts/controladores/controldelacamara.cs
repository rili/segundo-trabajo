using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controldelacamara : MonoBehaviour
{
    public float giroF = 120;
    private float Giro;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // flechas
        float horizontalInput = Input.GetAxis("Debug Horizontal");
        float verticalInput = Input.GetAxis("Debug Vertical");


        // rotacion movimiento
        Giro += horizontalInput * giroF * Time.deltaTime;
        float pitch = Mathf.Lerp(0, 45, Mathf.Abs(verticalInput)) * Mathf.Sign(verticalInput);
        float roll = Mathf.Lerp(0, 45, Mathf.Abs(horizontalInput)) * -Mathf.Sign(horizontalInput);

        transform.localRotation = Quaternion.Euler(Vector3.up * Giro + Vector3.right * pitch + Vector3.forward * roll);
    }
}
