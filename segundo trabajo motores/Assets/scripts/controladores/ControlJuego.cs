using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject personaje;
    public GameObject pantallagameover;
    public TMPro.TMP_Text textocontadorconometro;
    public Camera primeracamara;
    public Camera atrascamara;
    public static float tiempoRestante;
    public float duracion;
    public float cuenta;


    void Start()
    {
        StartCoroutine(ComenzarCronometro(duracion));
        Time.timeScale = 1;
        
    }

    void Update()
    {
        setearTextos();

        if (tiempoRestante == 0)
        {
            pantallagameover.SetActive(true);
            Time.timeScale = 0;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            cambiarcamara();
        }
    }


    public void cambiarcamara()
    { 
        if (primeracamara.enabled == true)
        {
            atrascamara.enabled = true;
            primeracamara.enabled = false;

        }
        else if (primeracamara.enabled == false)
        {
            primeracamara.enabled = true;
            atrascamara.enabled = false;

        }
    }


    public IEnumerator ComenzarCronometro(float valorCronometro)
    {
        tiempoRestante = valorCronometro;
        while (tiempoRestante > 0)
        {
            Debug.Log("Restan " + tiempoRestante + " segundos.");
            yield return new WaitForSeconds(1.0f);
            tiempoRestante--;
        }
    }
    public IEnumerator Comenzarcontador()
    {
        while (tiempoRestante != 0)
        {
            Debug.Log("pasaron " + cuenta + " segundos.");
            yield return new WaitForSeconds(1.0f);
            cuenta++;
        }
    }
    private void setearTextos()
    {
        textocontadorconometro.text = "cronometro: " + tiempoRestante.ToString();
    }
}

