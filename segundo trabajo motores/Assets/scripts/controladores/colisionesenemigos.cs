using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colisionesenemigos : MonoBehaviour
{
    public GameObject explosion;
    public GameObject yo;
    public int tiga;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("piso") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            ControlJuego.tiempoRestante += tiga;
            Destroy(kaboom, 2f);
            Destroy(yo,0.8f);

        }
        if (other.gameObject.CompareTag("enemigo") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            ControlJuego.tiempoRestante += tiga;
            Destroy(kaboom, 2f);
            Destroy(yo, 0.8f);
        }
        if (other.gameObject.CompareTag("misil") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            ControlJuego.tiempoRestante += tiga;
            Destroy(kaboom, 2f);
            Destroy(yo, 0.8f);
        }
        if (other.gameObject.CompareTag("jugador ") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            Destroy(kaboom, 2f);
            Destroy(yo, 0.8f);
        }
        if (other.gameObject.CompareTag("bala") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            ControlJuego.tiempoRestante += tiga;
            Destroy(kaboom, 2f);
            Destroy(yo, 0.8f);
        }
        if (other.gameObject.CompareTag("laser") == true)
        {
            GameObject kaboom = Instantiate(explosion, yo.transform.position, Quaternion.identity, yo.transform);
            ControlJuego.tiempoRestante += tiga;
            Destroy(kaboom, 2f);
            Destroy(yo, 0.8f);
        }
    }
}
