using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tactperseguir : MonoBehaviour
{
    public GameObject se�al1;
    public GameObject se�al2;
    public GameObject se�al3;
    public GameObject se�al4;
    public GameObject se�al5;
    public GameObject se�al6;
    public GameObject se�al7;
    public GameObject se�al8;
    public GameObject objetivo;
    public int rapidez;
    public int ra;

    void Start()
    {
        se�al1 = GameObject.Find("se�al1");
        se�al2 = GameObject.Find("se�al2");
        se�al3 = GameObject.Find("se�al3");
        se�al4 = GameObject.Find("se�al4");
        se�al5 = GameObject.Find("se�al5");
        se�al6 = GameObject.Find("se�al6");
        se�al7 = GameObject.Find("se�al7");
        se�al8 = GameObject.Find("se�al8");

        GameObject[] listas = new GameObject[8];
        listas[0] = se�al1;
        listas[1] = se�al2;
        listas[2] = se�al3;
        listas[3] = se�al4;
        listas[4] = se�al5;
        listas[5] = se�al6;
        listas[6] = se�al7;
        listas[7] = se�al8;

        int randomindex = (Random.Range(0, listas.Length));
        objetivo = (listas[randomindex]);
        ra = randomindex;

    }



    private void Update()
    {
        if (objetivo != null)
        {
            transform.LookAt(objetivo.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
        else
        {
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
        if (controlavion.marcha == 0)
        {
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
    }
}