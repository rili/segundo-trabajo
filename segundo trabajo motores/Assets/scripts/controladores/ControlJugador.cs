using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ControlJugador : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidez;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public BoxCollider col;
    private int saltos = 2;
    public TMPro.TMP_Text textoCantidadRecolectados;
    public TMPro.TMP_Text textovelocidad;
    public TMPro.TMP_Text textoGanaste;
    private int cont;
    public int vida;
    public GameObject pantallagameover;
    public GameObject control;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<BoxCollider>();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();
        Cursor.lockState = CursorLockMode.Locked;
    }


    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);

        transform.position += (vectorMovimiento * rapidez);
    }

    private void update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

    }

    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Hongos combustible: " + cont.ToString();
        if (cont >= 5)
        {
            Time.timeScale = 0;
            textoGanaste.text = "Ganaste!";
        }
        textovelocidad.text = "k/h:  " + rapidez.ToString();
    }


    private void Update()
    {
        int fullsaltos = 2;
        if (EstaEnPiso())
        {
            saltos =  fullsaltos;
        }

        if (Input.GetKeyDown(KeyCode.Space) && saltos>1)
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            saltos = saltos - 1;
        }
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.size.y * .15f, capaPiso);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("power up") == true)
        {
            float x = 0.05f;
            float y = 0.05f;
            float z = 0.05f;
            float aceleracion = 0.02f;
            transform.localScale += new Vector3(x,y,z);
            rapidez += aceleracion;
            magnitudSalto = magnitudSalto + 1;
            cont = cont + 1;  
            setearTextos();
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("caida") == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (other.gameObject.CompareTag("enemigo") == true)
        {
            pantallagameover.SetActive(true);
            Time.timeScale = 0;
        }
        if (other.gameObject.CompareTag("reloj") == true)
        {
            ControlJuego.tiempoRestante += 5;
            other.gameObject.SetActive(false);
        }
    }
}