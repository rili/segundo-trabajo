using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class radio : MonoBehaviour
{
    public static int cancion;
    private bool hecho;
    
    // Start is called before the first frame update
    void Start()
    {
        hecho = true;
        cancion = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown("h")) && (cancion == 0))
        {
            cancion++;
            hecho = true;
        }

        //gestor de radio
        if ((Input.GetKeyDown("k")) && (cancion > 0) && (cancion<7))
        {
            cancion++;
            hecho = true;
        }
        if ((Input.GetKeyDown("j")) && (cancion > 1) && (cancion < 8))
        {
            cancion--;
            hecho = true;
        }


        //play canciones
        if ((cancion ==0))
        {
            if (hecho ==true)
            {
                GestorDeAudio.instancia.ReproducirSonido("elevador");
                hecho = false;
            }
            
        }
        if ((cancion == 1))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("elevador");
                GestorDeAudio.instancia.PausarSonido("adrenaline");
                GestorDeAudio.instancia.ReproducirSonido("skyrider");
                hecho = false;
            }
         
        }
        if ((cancion == 2))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("skyrider");
                GestorDeAudio.instancia.PausarSonido("spitfire");
                GestorDeAudio.instancia.ReproducirSonido("adrenaline");
                hecho = false;
            }
        }
        if ((cancion == 3))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("adrenaline");
                GestorDeAudio.instancia.PausarSonido("thetop");
                GestorDeAudio.instancia.ReproducirSonido("spitfire");
                hecho = false;
            }
        }
        if ((cancion == 4))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("spitfire");
                GestorDeAudio.instancia.PausarSonido("speedlover");
                GestorDeAudio.instancia.ReproducirSonido("thetop");
                hecho = false;
            }

        }
        if ((cancion == 5))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("thetop");
                GestorDeAudio.instancia.PausarSonido("crazylove");
                GestorDeAudio.instancia.ReproducirSonido("speedlover");
                hecho = false;
            }

        }
        if ((cancion == 6))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("speedlover");
                GestorDeAudio.instancia.PausarSonido("risingsun");
                GestorDeAudio.instancia.ReproducirSonido("crazylove");
                hecho = false;
            }
        }
        if ((cancion == 7))
        {
            if (hecho == true)
            {
                GestorDeAudio.instancia.PausarSonido("crazylove");
                GestorDeAudio.instancia.ReproducirSonido("risingsun");
                hecho = false;
            }
        }

    }
}
