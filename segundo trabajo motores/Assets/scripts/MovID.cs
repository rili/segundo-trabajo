using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovID : MonoBehaviour
{
    public float rapidez;
    public float strength;

    private float randomOffset;

    void Start()
    {
        randomOffset = Random.Range(0f, 2f);
    }

    void Update()
    {
        Vector3 pos = transform.position;
        pos.x = Mathf.Sin(Time.time * rapidez + randomOffset) * strength;
        transform.position = pos;
    }
}
