using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomspawner : MonoBehaviour
{
    public GameObject pre1;
    public GameObject pre2;
    public GameObject pre3;
    public GameObject pre4;
    public GameObject pre5;
    public GameObject pre6;
    public GameObject enemigo;
    public GameObject yo;
    public float Radio;
    public int rand;
    public int rant;
    private bool hecho;
    public float tiempo;

    // Start is called before the first frame update
    void Start()
    {
        GameObject ene1 = pre1;
        GameObject ene2 = pre2;
        GameObject ene3 = pre3;
        GameObject ene4 = pre4;
        GameObject ene5 = pre5;
        GameObject ene6 = pre6;


        GameObject[] listas = new GameObject[6];
        listas[0] = ene1;
        listas[1] = ene2;
        listas[2] = ene3;
        listas[3] = ene4;
        listas[4] = ene5;
        listas[5] = ene6;

        sacarenemigo(listas);
        sacartiempo();
        Invoke("randomspawn", tiempo);
 
    }

    // Update is called once per frame
    void Update()
    {
    }


    public void sacarenemigo(GameObject[] listas) 
    {
        int rand = (Random.Range(0, listas.Length));
        enemigo = (listas[rand]);
    }

    public void sacartiempo()
    {
        int tiempo = (Random.Range(120, 240));
    }


    public void randomspawn()
    {
        GameObject ene1 = pre1;
        GameObject ene2 = pre2;
        GameObject ene3 = pre3;
        GameObject ene4 = pre4;
        GameObject ene5 = pre5;
        GameObject ene6 = pre6;


        GameObject[] listas = new GameObject[6];
        listas[0] = ene1;
        listas[1] = ene2;
        listas[2] = ene3;
        listas[3] = ene4;
        listas[4] = ene5;
        listas[5] = ene6;


        sacarenemigo(listas);

        Vector3 randompo = ((Random.insideUnitSphere * Radio) + yo.transform.position);
        Instantiate(enemigo, randompo, Quaternion.identity);

        sacartiempo();
        Invoke("randomspawn", tiempo);

    }

    private void OnGizmo()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, Radio);
    }
}
